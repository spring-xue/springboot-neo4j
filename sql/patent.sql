/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50735
 Source Host           : localhost:3306
 Source Schema         : patent

 Target Server Type    : MySQL
 Target Server Version : 50735
 File Encoding         : 65001

 Date: 18/04/2022 18:02:20
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `loginname` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `address` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `sex` int(11) DEFAULT NULL,
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `pwd` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `available` int(11) DEFAULT 1,
  `ordernum` int(11) DEFAULT NULL,
  `type` int(255) DEFAULT NULL COMMENT '用户类型[0管理员1项目经理]',
  `imgpath` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '头像地址',
  `phone` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `managerId` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1, '超级管理员', 'admin', '系统深处的男人', 1, '超级管理员', '1', NULL, 1, 1, 0, '../resources/images/defaultusertitle.jpg', NULL, NULL);
INSERT INTO `user` VALUES (2, '李四', 'ls', '武汉', 0, 'KING', '1', NULL, 1, 2, 1, '../resources/images/defaultusertitle.jpg', NULL, NULL);
INSERT INTO `user` VALUES (3, '王五', 'ww', '武汉', 1, '管理员', '123456', '2', 1, 3, 1, '../resources/images/defaultusertitle.jpg', NULL, NULL);
INSERT INTO `user` VALUES (4, '赵六', 'zl', '武汉', 1, '程序员', '123456', '3', 1, 4, 1, '../resources/images/defaultusertitle.jpg', NULL, NULL);
INSERT INTO `user` VALUES (10, '卷王', '1', NULL, NULL, NULL, '1', NULL, 1, NULL, 1, NULL, NULL, '1');
INSERT INTO `user` VALUES (11, '郭美美', '2', NULL, NULL, NULL, '2', NULL, 1, NULL, 1, NULL, NULL, '3');
INSERT INTO `user` VALUES (12, '郝强', '4', NULL, NULL, NULL, '4', NULL, 1, NULL, 1, NULL, NULL, '4');
INSERT INTO `user` VALUES (13, '谢老二', '12', NULL, NULL, NULL, '12', NULL, 1, NULL, 1, NULL, NULL, '6');

SET FOREIGN_KEY_CHECKS = 1;

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>专利列表</title>
    <link rel="stylesheet" href="${request.contextPath}/layuiadmin/layui/css/layui.css" media="all">
    <style>


    </style>
</head>
<body style="background-color: #F2F2F2;" >
<div style="padding: 20px; ">
    <div class="layui-row layui-col-space15">
        <div class="layui-col-md12">
            <div class="layui-card">
                <div class="layui-card-body">
                    <form class="layui-form" action="" style="margin-top: 8px" lay-filter="dataForm">
                        <div class="layui-form-item">
                            <div class="layui-inline">
                                <label class="layui-form-label">标题</label>
                                <div class="layui-input-inline">
                                    <input  type="text" name="title" lay-verify="title" autocomplete="off" placeholder="" class="layui-input">
                                </div>
                                <div class="layui-input-inline">
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <button type="button" class="layui-btn" id="search">查询</button>
                                    <button type="reset" class="layui-btn" id="reset">清空</button>
                                </div>
                            </div>


                        </div>
                    </form>
                    <#-- 数据表格-->
                    <table class="layui-hide" id="table" lay-filter="table"></table>
                    <script type="text/html" id="toolbar">
                        <div class="layui-btn-container">
                            <button class="layui-btn layui-btn-sm" lay-event="add">上传</button>
                            <a class="layui-btn layui-btn-sm layui-btn-normal" href="${request.contextPath}/genPublic" >公开号</a>
                            <a class="layui-btn layui-btn-sm layui-btn-normal" href="${request.contextPath}/genApply">申请号</a>
                            <a class="layui-btn layui-btn-sm layui-btn-normal" href="${request.contextPath}/genTitle">标题</a>
                            <a class="layui-btn layui-btn-sm layui-btn-normal" href="${request.contextPath}/genAbstract">摘要</a>
                            <a class="layui-btn layui-btn-sm layui-btn-normal" href="${request.contextPath}/genClassify">分类号</a>
                            <a class="layui-btn layui-btn-sm layui-btn-normal" href="${request.contextPath}/genDescription">方案描述</a>
                            <a class="layui-btn layui-btn-sm layui-btn-normal" href="${request.contextPath}/genClaim">权利要求</a>
                            <a class="layui-btn layui-btn-sm layui-btn-normal" href="${request.contextPath}/genApplicants">申请人</a>
                            <a class="layui-btn layui-btn-sm layui-btn-normal" href="${request.contextPath}/genInventors">发明人</a>
                            <a class="layui-btn layui-btn-sm layui-btn-normal" href="${request.contextPath}/genLog">log</a>
                        </div>
                    </script>

                    <script type="text/html" id="bar">
                        <a class="layui-btn layui-btn-xs" href="patentList.ftl" target="_blank">查看</a>
                        <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
                    </script>

                </div>
            </div>
        </div>
    </div>
</div>


<script src="${request.contextPath}/layuiadmin/layui/layui.js"></script>

<script>
    layui.use(['table','form','jquery','laydate'], function(){
        var table = layui.table;
        var form=layui.form;
        var $=layui.$;
        var layer=layui.layer;
        var laydate=layui.laydate;

        var param={}
        var userId=""
        if('${(user.type)!}'==='1'){
            userId='${(user.id)!}'
        }
        //日期选择
        laydate.render({
            elem: '#year'
            ,trigger: 'click'//呼出事件改成click
            ,type: 'year'
        });

        var addLayerIndex='';
        table.render({
            elem: '#table'
            ,url:'${request.contextPath}/getPageList'
            ,method:'post'
            ,contentType: "application/json;charset=UTF-8"
            ,dataType: 'json'
            ,done:function (res,curr,count) {
                // console.log(res)
                // $("tr").css("height",60)
            }
            ,toolbar: '#toolbar' //开启头部工具栏，并为其绑定左侧模板
            ,defaultToolbar: [
                //     'filter', 'exports', 'print', { //自定义头部工具栏右侧图标。如无需自定义，去除该参数即可
                //     title: '提示'
                //     ,layEvent: 'LAYTABLE_TIPS'
                //     ,icon: 'layui-icon-tips'
                // }
            ]
            ,title: 'table'
            ,cols: [[
                // {field:'id',title:'序号'},
                {field:'title',title:'题目'}
                ,{field:'description',title:'描述'}
                ,{field:'enAbstract', title:'摘要'}
                ,{field:'publicNum', title:'公开号',templet:formatPublic}
                ,{field:'applyNum',title:'申请号',templet:formatApply}
                ,{field:'datesOfPublicAvailability',title:'公开日期'}
                ,{fixed: 'right', title:'操作',width:130,templet:dynamicButton}
            ]]
            ,page: true
            ,parseData: function(res){ //res 即为原始返回的数据
                console.log(res)
                return {
                    "code": res.data.code, //解析接口状态
                    "msg": res.data.msg, //解析提示文本
                    "count": res.data.count, //解析数据长度
                    "data": res.data.data //解析数据列表
                };
            },
        });

        // 格式化公开号信息
        function formatPublic(d) {
            if(d.publicNum!==undefined&&d.publicNum!==null){
                var document=JSON.parse(d.publicNum)
                return document.docNumber
            }else {
                return ''
            }

        }
        // 格式化申请号
        function formatApply(d) {
            if(d.applyNum!==undefined&&d.applyNum!==null){
                var document=JSON.parse(d.applyNum)
                return document.docNumber
            }else {
                return ''
            }

        }

        function dynamicButton(d) {
            var html='<a class="layui-btn layui-btn-xs" href="/toInfo?id='+d.id+'" target="_blank">查看</a>' +
                '<a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>';


            return html;
        }
        //头工具栏事件
        table.on('toolbar(table)', function(obj){
            var checkStatus = table.checkStatus(obj.config.id);


            switch(obj.event){
                case 'getCheckData':
                    var data = checkStatus.data;
                    console.log(data)
                    layer.alert(JSON.stringify(data));
                    break;
                case 'getCheckLength':
                    var data = checkStatus.data;
                    layer.msg('选中了：'+ data.length + ' 个');
                    break;
                case 'isAll':
                    layer.msg(checkStatus.isAll ? '全选': '未全选');
                    break;

                //自定义头工具栏右侧图标 - 提示
                case 'LAYTABLE_TIPS':
                    layer.alert('这是工具栏右侧自定义的一个图标按钮');
                    break;
                    break;
                //自定义头工具栏右侧图标 - 提示
                case 'add':
                    layer.open({
                        title:'测试',
                        type: 2,
                        area: ['500px', '210px'],
                        // area: '30%',
                        content: ['${request.contextPath}/toUpload'], //这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['http://sentsin.com', 'no']
                        btn: ['确认','取消'], //按钮
                        yes:function (index,layero) {
                            addLayerIndex=index;
                            // var son = window['layui-layer-iframe' + index];
                            // son.child(111);
                            //获取子窗口的函数
                            var formSubmit=layer.getChildFrame('form', index);
                            console.log(formSubmit)
                            var submited = formSubmit.find('button')[0];
                            submited.click()
                        }
                    });
                    break;
                //批量删除
                case 'multiDelete':

                    layer.confirm('确认删除选中的数据吗？', function(index){
                        var data = checkStatus.data;
                        var idAry=[];
                        if(data.length===0){
                            layer.msg("请至少选择一条数据！！");
                            return false;
                        }
                        for(var i=0;i<data.length;i++){
                            var row=data[i];
                            idAry.push(row.id);
                        }
                        var param={}
                        param.table='${page}';
                        param.ids=idAry;
                        $.ajax({
                            type:"post",
                            url: "${request.contextPath}/deleteItemsByIds",
                            //这里没有用json传值，所以contentType
                            contentType:'application/json; charset=UTF-8',
                            data : JSON.stringify(param),
                            success: function(res) {
                                if(res.code===200){
                                    layer.msg(res.msg)
                                    layer.close(index);
                                    table.reload('table')
                                }
                            },
                            error: function(res) {
                                layer.alert(res.msg,{icon:2})
                            }
                        })
                        layer.close(index);
                    });
            };
        });

        //监听行工具事件
        table.on('tool(table)', function(obj){
            var data = obj.data;
            var idAry=[];
            idAry.push(data.id);
            var param={}
            param.ids=idAry;
            param.table='${page}'

            if(obj.event === 'del'){
                layer.confirm('确认删除吗？', function(index){
                    $.ajax({
                        type:"post",
                        url: "${request.contextPath}/delById?id="+data.id,
                        //这里没有用json传值，所以contentType
                        contentType:'application/json; charset=UTF-8',
                        // data :JSON.stringify(param),
                        success: function(res) {
                            if(res.code===200){
                                layer.msg(res.msg)
                                layer.close(index);
                                table.reload('table')
                            }
                        },
                        error: function(res) {
                            layer.alert(res.msg,{icon:2})
                        }
                    })
                    layer.close(index);
                });
            }else if(obj.event === 'edit'){
                console.log(data)
                layer.open({
                    title:'修改信息',
                    type: 2,
                    area: ['500px', '450px'],
                    // area: '30%',
                    content: ['${request.contextPath}/to/${page}?type=layer&id='+data.id], //这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['http://sentsin.com', 'no']
                    btn: ['确认','取消'], //按钮
                    yes:function (index,layero) {
                        addLayerIndex=index;
                        // var son = window['layui-layer-iframe' + index];
                        // son.child(111);
                        //获取子窗口的函数
                        var formSubmit=layer.getChildFrame('form', index);
                        console.log(formSubmit)
                        var submited = formSubmit.find('button')[0];
                        submited.click()

                    }
                });


            }
        });

        //查询按钮
        $("#search").click(function () {
            var title=$("input[name='title']").val();
            var applyNum=$("input[name='applyNum']").val();
            //列表的重载
            table.reload('table', {
                    page:{curr:1}
                    ,where: {title: title,applyNum:applyNum}
                }
            );


        });


        //关闭新增layer
        window.closeAddLayer=function () {
            layer.close(addLayerIndex)
        }
        //重载客户列表
        window.reloadTable=function(){
            table.reload('table')
        }
    });

</script>
<script type="text/html" id="imgtmp">
    <img src="{{d.headimgurl}}" style="height: 50px;width: 50px">
</script>
</body>
</html>
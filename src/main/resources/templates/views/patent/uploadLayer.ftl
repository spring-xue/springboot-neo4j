<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>上传文件</title>
    <link rel="stylesheet" href="${request.contextPath}/layuiadmin/layui/css/layui.css" media="all">
</head>
<style>
</style>
<body style="background-color: #F2F2F2;" >
<div style="padding: 20px; ">
    <div class="layui-row layui-col-space15">
        <div class="layui-col-md12">
            <div class="layui-card">
                <div class="layui-card-body">
                    <div class="layui-btn-container">
                        <button type="button" class="layui-btn" id="test3"><i class="layui-icon"></i>上传文件</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="${request.contextPath}/layuiadmin/layui/layui.js"></script>

<script>
    layui.config({
        base: '${request.contextPath}/layuiadmin/modules/' //静态资源所在路径
    }).extend({
        xmSelect: 'xm-select'
    }).use(['upload','form','jquery','laydate','xmSelect'], function(){
        var form=layui.form;
        var $=layui.$;
        var laydate = layui.laydate;
        var upload = layui.upload

        //日期选择
        laydate.render({
            elem: '#registrationTime'
            ,trigger: 'click'//呼出事件改成click
            ,type: 'date'
        });
        //日期选择
        laydate.render({
            elem: '#safekeepingTime'
            ,trigger: 'click'//呼出事件改成click
            ,type: 'date'
        });

        upload.render({
            elem: '#test3'
            ,url: '${request.contextPath}/file/fileUpload' //此处配置你自己的上传接口即可
            ,accept: 'file' //普通文件
            ,done: function(res){
                parent.layer.alert('上传成功', { icon: 1, closeBtn: 0 }, function (index) {
                    parent.layer.closeAll()
                    parent.reloadTable();
                });
            }
        });


        window.closeLayer=function(index){
            layer.close(index)
        }


    });
</script>

</body>
</html>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>专利详情</title>
    <link rel="stylesheet" href="${request.contextPath}/layuiadmin/layui/css/layui.css" media="all">
</head>
<style>
    .title{
        font-size: 25px;
        font-weight: bolder;
        margin-bottom: 10px;!important;
    }
    .content{
        font-size: 20px;
    }
</style>
<body style="background-color: #F2F2F2;" >
<div style="padding: 20px; ">
    <div class="layui-row layui-col-space15">
        <div class="layui-col-md12">
            <div class="layui-card">
                <div class="layui-card-body">
                    <br>
                    <span class="title" style="margin-top: 10px">标题:</span><span class="content" id="title"></span><br><br>
                    <span class="title" style="margin-top: 10px">公开号:</span><span class="content" id="publicNum"></span><br><br>
                    <span class="title" style="margin-top: 10px">申请号:</span><span class="content" id="applyNum"></span><br><br>
                    <span class="title" style="margin-top: 10px">公开日期:</span><span class="content" id="datesOfPublicAvailability"></span><br><br>
                    <span class="title" style="margin-top: 10px">授权日期:</span><span class="content" id="termOfGrant"></span><br><br>
                    <span class="title" style="margin-top: 10px">引用:</span><span class="content" id="citation"></span><br><br>
                    <span class="title" style="margin-top: 10px">申请人:</span><span class="content" id="applicants"></span><br><br>
                    <span class="title" style="margin-top: 10px">发明人:</span><span class="content" id="inventors"></span><br><br>
                    <span class="title" style="margin-top: 10px">ipcr:</span><span class="content" id="ipcrs"></span><br><br>
                    <span class="title" style="margin-top: 10px">ecla:</span><span class="content" id="ecla"></span><br><br>
                    <span class="title" style="margin-top: 10px">权利要求:</span><span class="content" id="claim"></span><br><br>
                    <span class="title" style="margin-top: 10px">英文摘要:</span><span class="content" id="enAbstract"></span><br><br>
                    <span class="title" style="margin-top: 10px">英文描述:</span><span class="content" id="description"></span><br><br>
                    <span class="title" style="margin-top: 10px">版权:</span><span class="content" id="copyright"></span><br><br>

                </div>
            </div>
        </div>
    </div>
</div>
<script src="${request.contextPath}/layuiadmin/layui/layui.js"></script>

<script>
    layui.config({
        base: '${request.contextPath}/layuiadmin/modules/' //静态资源所在路径
    }).extend({
        xmSelect: 'xm-select'
    }).use(['upload','form','jquery','laydate','xmSelect'], function(){
        var form=layui.form;
        var $=layui.$;
        $.ajax({
            type:"post",
            url: "${request.contextPath}/getById?id=${id}",
            contentType:'application/json; charset=UTF-8',
            success: function(res) {
                console.log("=====success=====")
                console.log(res)
                $("#title").text(res.data.title)
                $("#publicNum").text(JSON.parse(res.data.publicNum).docNumber)
                $("#applyNum").text(JSON.parse(res.data.applyNum).docNumber)
                if(res.data.datesOfPublicAvailability!==null){
                    $("#datesOfPublicAvailability").text(res.data.datesOfPublicAvailability)
                }


                var termOfGrantList=JSON.parse(res.data.termOfGrant)
                var termOfGrantStr="";
                if(termOfGrantList!=null&&termOfGrantList!==undefined){
                    for(var i=0;i<termOfGrantList.length;i++){
                        var item=termOfGrantList[i];
                        if(i<termOfGrantList.length){
                            termOfGrantStr=termOfGrantStr+'country:'+item.country+',date:'+item.date+','
                        }else {
                            termOfGrantStr=termOfGrantStr+'country:'+item.country+'date:'+item.date
                        }

                    }
                    $("#termOfGrant").text(termOfGrantStr)
                }

                var citation =JSON.parse(res.data.citation)
                if(citation!==null&&citation!==undefined){
                    var patcits=citation.patcits
                    var citationList=""

                    for(var i=0;i<patcits.length;i++){
                        var item=patcits[i];
                        if(i<patcits.length){
                            citationList=citationList+'country:'+item.country+',docNumber:'+item.docNumber+',kind:'+item.kind+','
                        }else {
                            citationList=citationList+'country:'+item.country+',docNumber:'+item.docNumber+',kind:'+item.kind
                        }

                    }
                    $("#citation").text(citationList)
                }


                var applicants =res.data.applicants
                var applicantStr=""
                for(var i=0;i<applicants.length;i++){
                    var item=JSON.parse(applicants[i]);
                        if(item.name===undefined){
                            applicantStr=applicantStr+'lastName:'+item.lastName+','
                        }else {
                            applicantStr=applicantStr+'name:'+item.name+','
                        }
                }
                $("#applicants").text(applicantStr)



                var inventors =res.data.inventors
                var inventorStr=""
                for(var i=0;i<inventors.length;i++){
                    var item=JSON.parse(inventors[i]);
                    if(item.name===undefined){
                        inventorStr=inventorStr+'lastName:'+item.lastName+','
                    }else {
                        inventorStr=inventorStr+'name:'+item.name+','
                    }
                }
                $("#inventors").text(inventorStr)

                var ipcrs =res.data.ipcrs
                if(ipcrs!==null&&ipcrs!==undefined){
                    var ipcrsStr=""
                    for(var i=0;i<ipcrs.length;i++){
                        var itemArr=ipcrs[i].split(" ");

                        ipcrsStr=ipcrsStr+itemArr[0]+" "+itemArr[1]+','
                    }
                    $("#ipcrs").text(ipcrsStr)

                }

                var ecla =res.data.ecla
                if(ecla!==null &&ecla!==undefined){
                    var eclaStr=""
                    for(var i=0;i<ecla.length;i++){
                        var item=ecla[i];
                        eclaStr=eclaStr+item+','
                    }
                    $("#ecla").text(eclaStr)
                }

                var claim =res.data.claim
                if(claim!==null &&claim!==undefined){
                    var claimStr=""
                    for(var i=0;i<claim.length;i++){
                        var item=claim[i];
                        claimStr=claimStr+item+','
                    }
                    $("#claim").text(claimStr)
                }
                if(res.data.enAbstract!=null&&res.data.enAbstract!==undefined){
                    $("#enAbstract").text(res.data.enAbstract)
                }

                var description =res.data.description
                if(description!==null &&description!==undefined){
                    var descriptionStr=""
                    for(var i=0;i<description.length;i++){
                        var item=description[i];
                        descriptionStr=descriptionStr+item+','
                    }
                    $("#description").text(descriptionStr)
                }

                if(res.data.copyright!=null&&res.data.copyright!==undefined){
                    $("#copyright").text(res.data.copyright)
                }
            },
            error: function(res) {
                layer.alert(res.msg,{icon:2})
            }
        })


    });
</script>

</body>
</html>
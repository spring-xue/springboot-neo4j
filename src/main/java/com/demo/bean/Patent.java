package com.demo.bean;

import lombok.Data;
import org.neo4j.ogm.annotation.GeneratedValue;
import org.neo4j.ogm.annotation.Id;

import java.util.List;
import java.util.Map;

/**
 * 专利
 */
@Data
public class Patent {
    @Id
    @GeneratedValue
    private Long id;
//    公开号
    public String publicNum;
//    申请号
    private String applyNum;
    //公开日期
    private String datesOfPublicAvailability;
    //授权日期
    private String termOfGrant;
    //英文标题
    private String title;
    //引用
    private String citation;
    //参与人
    private List<String> applicants;
    //发明人
    private List<String> inventors;
    //ipcr
    private List<String> ipcrs ;
    //ecla
    private List<String> ecla ;
    //权利要求
    private List<String> claim ;
    //英文摘要
    private String enAbstract;
    //英文描述
    private List<String> description ;
    //版权
    private String copyright ;

}

package com.demo.bean;

import lombok.Data;

/**
 * 地址簿
 */
@Data
public class AddressBook {
    private Long id;
    private String name;
    private String lastName;
    private String city;
    private String country;
}

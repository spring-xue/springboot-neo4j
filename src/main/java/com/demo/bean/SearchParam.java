package com.demo.bean;

import lombok.Data;

@Data
public class SearchParam {
    private Long id;
    private String title;
    private String describe;
    private String applyNum;
    private Integer page;
    private Integer limit;

}

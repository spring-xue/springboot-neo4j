package com.demo.bean;

import lombok.Data;

/**
 * 申请号
 */
@Data
public class BaseDocument {
    private Long id;
    private String country;
    private String docNumber;
    private String kind;
    private String date;

}

package com.demo.bean;

import lombok.Data;

import java.util.List;

/**
 * 引用
 */
@Data
public class Citation{
    private Long id;
    List<Patcit> patcits;
    List<String> nplcits;
}

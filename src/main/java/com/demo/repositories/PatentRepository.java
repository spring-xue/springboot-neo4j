package com.demo.repositories;

import com.demo.bean.Patent;

import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface PatentRepository extends Neo4jRepository<Patent,Long> {
    @Query(value = "MATCH (patent) WHERE patent.title Contains {title} and patent.applyNum={applyNum} return patent"
    ,countQuery = "MATCH (patent) WHERE patent.title Contains {title} and patent.applyNum={applyNum} return count(patent)")
    Page<Patent> getPageListByTitleAndapplyNum(@Param("title") String title,@Param("applyNum") String applyNum, Pageable pageable);

    @Query(value = "MATCH (patent) WHERE  patent.applyNum={applyNum} return patent"
            ,countQuery = "MATCH (patent) WHERE  patent.applyNum={applyNum} return count(patent)")
    Page<Patent> getPageListByapplyNum(@Param("applyNum") String applyNum, Pageable pageable);

    @Query(value = "MATCH (patent) WHERE patent.title Contains {title} return patent"
            ,countQuery = "MATCH (patent) WHERE patent.title Contains {title} return count(patent)")
    Page<Patent> getPageListByTitle(@Param("title") String title,Pageable pageable);

    @Query(value = "MATCH (patent) WHERE  patent.applyNum={applyNum} return patent")
    List<Patent> getPageListByApplyNum(@Param("applyNum") String applyNum);

}

package com.demo.service;

import com.demo.bean.Patent;
import com.demo.bean.SearchParam;

import java.util.List;


public interface PatentService {

    //查找全部
    List<Patent> getPageList(SearchParam searchParam);

}

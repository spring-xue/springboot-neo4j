package com.demo.service.serviceImpl;

import com.demo.bean.Patent;
import com.demo.bean.SearchParam;
import com.demo.repositories.PatentRepository;
import com.demo.service.PatentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;



@Service
@Transactional
public class PatentServiceImpl implements PatentService {

    @Autowired
    private PatentRepository repository;

    @Override
    public List<Patent> getPageList(SearchParam searchParam) {
        Pageable pageable=PageRequest.of(0,2);
        Page<Patent> all = repository.findAll(pageable, 0);

        return null;
    }


}

//package com.demo;
//
//
//import com.demo.bean.*;
//import com.demo.util.PatentUtil;
//import org.dom4j.Document;
//import org.dom4j.Element;
//import org.dom4j.io.SAXReader;
//
//import java.io.File;
//import java.util.ArrayList;
//import java.util.List;
//
//public class ParseXmlDemo {
//    public static void main(String[] args) throws Exception {
//        // TODO Auto-generated method stub
//        Patent patent=new Patent();
//        String path="D:\\project\\springboot-neo4j-master\\src\\main\\resources\\patent\\PAC-1003_EP-1324078-A2.xml";
//        String resultPath = PatentUtil.deleteDtd(path);
//
//        try {
//            File xmlFile=new File("D:\\project\\springboot-neo4j-master\\src\\main\\resources\\patent\\PAC-1003_EP-1324078-A2\\PAC-1003_EP-1324078-A2.xml");
//
//            // 实例化一个类用于添加xml文件
//
//            SAXReader reader = new SAXReader();
//            Document doc = reader.read(new File("D:\\project\\springboot-neo4j-master\\src\\main\\resources\\patent\\PAC-1003_EP-1324078-A2\\PAC-1003_EP-1324078-A2.xml"));
//            // 读取指定标签
//            //获取根标签
//            Element patentDocument=doc.getRootElement();
//
//            Element bibliographicDataElement =patentDocument.element("bibliographic-data");
//            //获取公开号
//            Element publicationReferenceElement =bibliographicDataElement .element("publication-reference");
//            Element document_id =publicationReferenceElement .element("document-id");
//            PublicationReference publicationReference=new PublicationReference();
//            List<Element> publicationReferenceElements = document_id.elements();
//            publicationReference.setCountry(publicationReferenceElements.get(0).getTextTrim());
//            publicationReference.setDocNumber(publicationReferenceElements.get(1).getTextTrim());
//            publicationReference.setKind(publicationReferenceElements.get(2).getTextTrim());
//            publicationReference.setDate(publicationReferenceElements.get(3).getTextTrim());
//            patent.setPublicationReference(publicationReference);
//            //获取申请号
//            Element applicationReferenceElement =bibliographicDataElement .element("application-reference");
//            document_id =applicationReferenceElement .element("document-id");
//            ApplicationReference applicationReference=new ApplicationReference();
//            List<Element> applicationReferenceElements = document_id.elements();
//            applicationReference.setCountry(applicationReferenceElements.get(0).getTextTrim());
//            applicationReference.setDocNumber(applicationReferenceElements.get(1).getTextTrim());
//            applicationReference.setKind(applicationReferenceElements.get(2).getTextTrim());
//            applicationReference.setDate(applicationReferenceElements.get(3).getTextTrim());
//            patent.setApplicationReference(applicationReference);
//            //获取公开日期
//            Element datesOfPublicAvailabilityElement = bibliographicDataElement.element("dates-of-public-availability");
//            if(datesOfPublicAvailabilityElement!=null){
//                Element intentionToGrantDateElement=datesOfPublicAvailabilityElement.element("intention-to-grant-date");
//                Element date=intentionToGrantDateElement.element("date");
//                patent.setDatesOfPublicAvailability(date.getTextTrim());
//            }
//            //获取授权日期
////            Element priorityClaimsElement =bibliographicDataElement .element("priority-claims");
//            Element termOfGrantElement=bibliographicDataElement.element("term-of-grant");
//            if(termOfGrantElement!=null){
//                List<Element> lapseOfPatentElementList = termOfGrantElement.elements("lapse-of-patent");
//                List<LapseOfPatent> lapseOfPatents=new ArrayList<>();
//                for(int i=0;i<lapseOfPatentElementList.size();i++){
//                    LapseOfPatent lapseOfPatent=new LapseOfPatent();
//                    Element lapseOfPatentElement=lapseOfPatentElementList.get(i);
//                    document_id=lapseOfPatentElement.element("document-id");
//                    lapseOfPatent.setCountry(document_id.element("country").getTextTrim());
//                    lapseOfPatent.setDate(document_id.element("date").getTextTrim());
//                    lapseOfPatents.add(lapseOfPatent);
//                }
//                patent.setTermOfGrant(lapseOfPatents);
//            }
//
//            //获取英文标题
//            Element technicalDataElement = bibliographicDataElement.element("technical-data");
//            List<Element>titleElementList=technicalDataElement.elements("invention-title");
//            for(int i=0;i<titleElementList.size();i++){
//                Element titleElement=titleElementList.get(i);
//                if("EN".equals(titleElement.attribute("lang").getData().toString())){
//                    patent.setTitle(titleElement.getTextTrim());
//                }
//            }
//            //获取引用
//            Element citationsElement = technicalDataElement.element("citations");
//            if(citationsElement!=null){
//                Element patentCitations=citationsElement.element("patent-citations");
//                List<Element> patcitElementList = patentCitations.elements("patcit");
//                List<Patcit> patcits=new ArrayList<>();
//                for(int i=0;i<patcitElementList.size();i++){
//                    Patcit patcit=new Patcit();
//                    Element patcitElement=patcitElementList.get(i);
//                    document_id = patcitElement.element("document-id");
//                    String country=document_id.element("country").getTextTrim();
//                    patcit.setCountry(country);
//                    String docNumber=document_id.element("doc-number").getTextTrim();
//                    patcit.setDocNumber(docNumber);
//                    String kind=document_id.element("kind").getTextTrim();
//                    patcit.setKind(kind);
//                    patcits.add(patcit);
//                }
//                Citation citation=new Citation();
//                citation.setPatcits(patcits);
//
//                Element nonPatentCitations=citationsElement.element("non-patent-citations");
//                Element nplcitElement=nonPatentCitations.element("nplcit");
//                String text=nplcitElement.element("text").getTextTrim();
//                List<String> nplcitList=new ArrayList<>();
//                nplcitList.add(text);
//                citation.setNplcits(nplcitList);
//                patent.setCitation(citation);
//            }
//
//            //获取ipcr
//            Element classificationsIpcr=technicalDataElement.element("classifications-ipcr");
//            List<Element> ipcrElementList=classificationsIpcr.elements("classification-ipcr");
//            List<String>ipcrs=new ArrayList<>();
//            for(int i=0;i<ipcrElementList.size();i++){
//                Element ipcr=ipcrElementList.get(i);
//                ipcrs.add(ipcr.getTextTrim());
//            }
//            patent.setIpcrs(ipcrs);
//            //获取ecla
//            Element classificationsEcla=technicalDataElement.element("classification-ecla");
//            List<Element> eclaElementList=classificationsEcla.elements("classification-symbol");
//            List<String>eclas=new ArrayList<>();
//            for(int i=0;i<eclaElementList.size();i++){
//                Element ecla=eclaElementList.get(i);
//                eclas.add(ecla.getTextTrim());
//            }
//            patent.setEcla(eclas);
//            //获取参与人
//            Element partiesElement = bibliographicDataElement.element("parties");
//            Element applicantsElement = partiesElement.element("applicants");
//            List<Element> applicantElementList=applicantsElement.elements("applicant");
//            List<Applicant>applicants=new ArrayList<>();
//            for(int i=0;i<applicantElementList.size();i++){
//                Applicant applicant=new Applicant();
//                Element applicantElement=applicantElementList.get(i);
//                Element addressBookElement=applicantElement.element("addressbook");
//                Element nameElement=addressBookElement.element("name");
//                if(nameElement!=null){
//                    applicant.setName(nameElement.getTextTrim());
//                }
//                Element lastNameElement=addressBookElement.element("last-name");
//                if(lastNameElement!=null){
//                    applicant.setLastName(lastNameElement.getTextTrim());
//                }
//                Element addressElement=addressBookElement.element("address");
//                if(addressElement!=null){
//                    Element cityElement=addressElement.element("city");
//                    if(cityElement!=null){
//                        applicant.setCity(cityElement.getTextTrim());
//                    }
//                    Element countryElement=addressElement.element("country");
//                    if(countryElement!=null){
//                        applicant.setCountry(countryElement.getTextTrim());
//                    }
//                }
//
//                applicants.add(applicant);
//            }
//            patent.setApplicants(applicants);
//            //获取发明人
//            Element inventorsElement = partiesElement.element("inventors");
//            List<Element>inventorElementList=inventorsElement.elements("inventor");
//            List<Inventor>inventors=new ArrayList<>();
//            for (int i=0;i<inventorElementList.size();i++){
//                Inventor inventor=new Inventor();
//                Element inventorElement=inventorElementList.get(i);
//                Element addressBookElement=inventorElement.element("addressbook");
//                Element nameElement=addressBookElement.element("name");
//                if(nameElement!=null){
//                    inventor.setName(nameElement.getTextTrim());
//                }
//                Element lastNameElement=addressBookElement.element("last-name");
//                if(lastNameElement!=null){
//                    inventor.setLastName(lastNameElement.getTextTrim());
//                }
//                Element addressElement=addressBookElement.element("address");
//                if(addressElement!=null){
//                    Element cityElement=addressElement.element("city");
//                    if(cityElement!=null){
//                        inventor.setCity(cityElement.getTextTrim());
//                    }
//                    Element countryElement=addressElement.element("country");
//                    if(countryElement!=null){
//                        inventor.setCountry(countryElement.getTextTrim());
//                    }
//                }
//                inventors.add(inventor);
//            }
//            patent.setInventors(inventors);
//            //获取英文摘要
//            List<Element> abstractElementList = patentDocument.elements("abstract");
//            for(int i=0;i<abstractElementList.size();i++){
//                Element abstractElement=abstractElementList.get(i);
//                if("EN".equals(abstractElement.attribute("lang").getData().toString())){
//                    String enAbstract=abstractElement.element("p").getTextTrim();
//                    patent.setEnAbstract(enAbstract);
//                }
//            }
//            //获取英文描述
//
//            List<Element> descriptionElementList = patentDocument.elements("description");
//            List<String> descriptionList=new ArrayList<>();
//            for(int i=0;i<descriptionElementList.size();i++){
//                Element descriptionElement=descriptionElementList.get(i);
////                找到英文的描述
//                if("EN".equals(descriptionElement.attribute("lang").getData().toString())){
//                    List<Element> elementList = descriptionElement.elements();
//                    for(int j=0;j<elementList.size();j++){
//                        Element element=elementList.get(j);
//                        descriptionList.add(element.getTextTrim());
//                    }
//                }else {
//                    continue;
//                }
//
//            }
//
//            patent.setDescription(descriptionList);
//            //获取版权
//            Element copyrightElement = patentDocument.element("copyright");
//            patent.setCopyright(copyrightElement.getTextTrim());
//            System.out.println(patent);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//    }
//
//    public static void  getElements(Element element){
//        List<Element> elements = element.elements();
//        if(elements.size()>0){
//            for (int i=0;i<elements.size();i++){
//                Element sonElement=elements.get(i);
//                System.out.println(sonElement.getName());
//                System.out.println(sonElement.getTextTrim());
//                getElements(elements.get(i));
//            }
//
//        }
//
//    }
//}

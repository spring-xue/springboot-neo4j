//package com.cqust.qj.config;
//
//
//import org.springframework.context.annotation.ComponentScan;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.data.neo4j.repository.Neo4jRepository;
//import org.springframework.data.neo4j.repository.config.EnableNeo4jRepositories;
//import org.springframework.transaction.annotation.EnableTransactionManagement;
//
///**
// * Created by DYP.CQUST on 2021/3/31
// */
//@Configuration
//@EnableNeo4jRepositories("com.cqust.qj.repositories")
//@EnableTransactionManagement
//@ComponentScan("com.cqust.qj.entity")
//public class PersistenceContext extends Neo4jAutoConfiguration {
//
//}
